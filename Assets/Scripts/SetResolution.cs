﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class SetResolution : MonoBehaviour {

    public GameObject soundmaingame_object;
    public AudioSource soundmaingame_audio_source;

    public IEnumerator checkInternetConnection(System.Action<bool> action)
    {
        WWW www = new WWW("http://google.com");
        yield return www;
        if (www.error != null)
        {
            action(false);
        }
        else
        {
            action(true);
        }
    }

    void Start()
    {
        // Switch to 640 x 480 full-screen
        PlayerPrefs.SetString("FirstPlay", System.DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));
        Screen.SetResolution(480, 800, true);

        if (!soundmaingame_audio_source.isPlaying)
        {
            soundmaingame_audio_source = soundmaingame_object.GetComponent<AudioSource>();
            soundmaingame_audio_source.GetComponent<AudioSource>().enabled = true;
            soundmaingame_audio_source.Play();
        }

        StartCoroutine(checkInternetConnection((isConnected) => {
            if (!isConnected) {
                SceneManager.LoadScene(2);
            }
        }));
    }
}
