﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;
using System.Linq;
using System.Text;
using UnityEngine.Networking;
using UnityEngine.EventSystems;
using GoogleMobileAds.Api;
using System;
using SimpleJSON;
using UnityEngine.SceneManagement;

public class Login : MonoBehaviour
{
    public string base_url = "http://bossgame.co.id/webServices/";
    public Collider2D SinglePlayerPlay;
    public GameObject FirstPage, SecondPage;
    public int statusPlay;
    public RawImage avatar2;
    public string bossgame_email;
    public string bossgame_username;
    public InputField bossgame_nickname;
    public string bossgame_iduser;
    public string bossgame_idgame;
    public string bossgame_avatar;
    public int bossgame_idvalue;
    public string bossgame_valmessage;

    public Text sisa_main, sisa_main_2_message, sisa_main_2, sisa_main_3, error_message_login, UsernameBossgame2;


    
    public string time_end_str;
    public string TimePlaying2;
    public Collider2D LoginBPlay;

    

    IEnumerator postGetProfile(string nickname)
    {
        WWWForm form = new WWWForm();
        form.AddField("nickname", nickname);
        form.AddField("gameID", "16");

        UnityWebRequest www = UnityWebRequest.Post(base_url + "cekNickName.php", form);
        yield return www.SendWebRequest();

        if (www.isNetworkError || www.isHttpError)
        {
            Debug.Log(www.error);
        }
        else
        {
            Debug.Log("Success Send API");
            string a = www.downloadHandler.text;
            Debug.Log(a);
            var jsonObject = JSON.Parse(a);
            bossgame_iduser = jsonObject["id"];
            PlayerPrefs.SetString("bossgame_iduser", jsonObject["id"]);
            bossgame_email = jsonObject["email"];
            bossgame_username = jsonObject["username"];
            PlayerPrefs.SetString("bossgame_username", jsonObject["username"]);
            bossgame_avatar = jsonObject["photo"];
            PlayerPrefs.SetString("bossgame_avatar", jsonObject["photo"]);
            bossgame_idvalue = jsonObject["value"];
            bossgame_valmessage = jsonObject["message"];
            PlayerPrefs.SetString("bossgame_heart", jsonObject["heart"]);
            if (bossgame_idvalue == 1)
            {
                this.statusPlay = 1;
                SceneManager.LoadScene(1);
            }
            else
            {
                error_message_login.text = bossgame_valmessage;
            }
            //Debug.Log(bossgame_iduser + bossgame_idgame);
            //StartCoroutine(LoadImgAvatar(base_url + "profile/" + bossgame_avatar));
        }
    }

    void Start()
    {
        
    }
    void OnMouseDown()
    {
        PlayerPrefs.SetString("bossgame_nickname", bossgame_nickname.text.ToString());
        StartCoroutine(postGetProfile(bossgame_nickname.text.ToString()));
    }

    public IEnumerator postPlayTimePlayingGame(string iu, string ig, string st, string et)
    {
        Login Udata = new Login();
        WWWForm form = new WWWForm();
        form.AddField("idUsers", iu);
        form.AddField("idGame", ig);
        form.AddField("startTime", st);
        form.AddField("endTime", et);

        UnityWebRequest www = UnityWebRequest.Post(Udata.base_url + "playGameEndpoint.php", form);
        yield return www.SendWebRequest();

        if (www.isNetworkError || www.isHttpError)
        {
            Debug.Log(www.error);
        }
        else
        {
            Debug.Log("Success Send API");
        }
    }
    



}
